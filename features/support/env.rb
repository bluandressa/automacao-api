require 'cucumber'
require 'httparty'
require 'httparty/request'
require 'httparty/response/headers'
require 'rspec'
require 'factory_bot'
require 'faker'
require 'json'
require 'pry'
require_relative 'spec_helper/rest'


World(REST)
