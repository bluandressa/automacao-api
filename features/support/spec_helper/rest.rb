module REST 
  def login
    Rest::Login.new
  end 

  def search_products
    Rest::Search.new
  end

  def register_user
    Rest::User.new
  end

  def register_product
    Rest::Product.new
  end
end