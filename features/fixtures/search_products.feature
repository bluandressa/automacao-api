#language: pt

Funcionalidade: Consultar Produto

  @search
  Cenario: Realizar consulta de produto
  Dado que o usuario efetue uma requisição na API GET
  Quando é retornado a mensagem "Listagem de produtos realizada com sucesso"
  Entao a lista de produtos é retornada