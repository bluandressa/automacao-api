module Rest
    class Search
      include HTTParty
  
      headers 'Content-Type' => 'application/json'
      headers 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvaWQiOiIyMTIyIiwidXN1YXJpb2xvZ2luIjoiYW5kcmVzc2Eucm9kcmlndWVzIiwidXN1YXJpb25vbWUiOiJBbmRyZXNzYSBSb2RyaWd1ZXMgIn0.u9KBWRQ6Urz47IvwpFKES_Eq4epcw-KaSqrFvHAY4uA'
      base_uri "http://165.227.93.41/lojinha"
      
      def get_product
        self.class.get("/produto")
      end
  
    end
  end