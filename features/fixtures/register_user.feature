#language: pt

Funcionalidade: Cadastro de Usuário

  @user
  Cenario: Realizar cadastro de usuario 
  Quando o usuario efetuar a requisição na API POST
  Entao é retornado "Usuário adicionado com sucesso"