#language: pt

Funcionalidade: Cadastro de Produto

  @register_product
  Cenario: Realizar cadastro de produto
  Quando efetuar a requisição na API POST
  Entao é retornado o "Produto adicionado com sucesso"