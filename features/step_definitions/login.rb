Quando('o usuario efetuar uma requisição POST para o endpoint Login') do
  @response = login.post_login
end
  
Entao('é retornado o status {int}') do |code|
  expect(@response.code).to eq code
end