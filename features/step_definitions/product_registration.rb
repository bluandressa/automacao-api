Quando('efetuar a requisição na API POST') do
    @products = register_product.post_product
end

Entao('é retornado o {string}') do |msg|
    expect(@products['message']).to eq msg
end